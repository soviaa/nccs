-- mysql -h 34.87.139.140 -u student -p
-- password is student

USE practice;

ALTER TABLE student ADD address VARCHAR(50) AFTER name;
DESCRIBE student;
SELECT * FROM student;

UPDATE student SET address = 'kathmandu'; -- all student will be set as kathmandu
SELECT * FROM student;
UPDATE student SET address = 'lalitpur' WHERE id='BIM1013'; --only student with id specified will be updated to lalitpur

SELECT * FROM result WHERE marks_obtained >= 60 and marks_obtained <= 80;
SELECT * FROM result WHERE marks_obtained between 60 and 80;

SELECT * FROM result WHERE marks_obtained in (60, 70, 88, 59);

-- wild cards for like % zero or more characters _ one character
SELECT * FROM student WHERE name LIKE '_a%'; -- select student whose second character in name is a
SELECT * FROM student WHERE name LIKE '%Maharjan';

SELECT count(id), address FROM student GROUP BY address;

SELECT student_id, subject, marks_obtained FROM result WHERE marks_obtained > pass_marks;
CREATE VIEW pass_list AS SELECT student_id, subject, marks_obtained FROM result WHERE marks_obtained > pass_marks;
SHOW tables; -- view is not a table it's a named query i.e. Query shortcut
SELECT * FROM pass_list;
SELECT * FROM pass_list WHERE subject='Math';

SELECT count(id), section FROM student GROUP BY section;

-- Insert your result data on result table
INSERT INTO result (subject, student_id, examdate, marks_obtained) VALUES 
('Database', 'NCCSBIM0990', now(), 78),
('Operating System', 'NCCSBIM0990', now(), 86),
('Mathematics', 'NCCSBIM0990', now(), 83);

-- student_id is foreign key of student tables primary key i.e. id so it should match with that value

SELECT MAX(marks_obtained) FROM result WHERE subject='Operating System';
SELECT * FROM result WHERE marks_obtained = (SELECT MAX(marks_obtained) FROM result WHERE subject='Operating System');

