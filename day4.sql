-- mysql -h 34.87.139.140 -u student -p
-- use password student

USE practice;
ALTER TABLE result ADD FOREIGN KEY (student_id) REFERENCES student(id);

-- insert your student and result data

SELECT * FROM RESULT ORDER BY marks_obtained;
SELECT * FROM RESULT ORDER BY marks_obtained desc;
SELECT * FROM RESULT ORDER BY marks_obtained desc, ID desc;

SELECT max(marks_obtained) FROM result WHERE student_id='076BIM443';
SELECT min(marks_obtained) FROM result WHERE student_id='076BIM443';
SELECT avg(marks_obtained) FROM result WHERE student_id='076BIM443';
SELECT sum(marks_obtained) FROM result WHERE student_id='076BIM443';

SELECT count(marks_obtained) FROM result WHERE marks_obtained > pass_mark;
SELECT count(marks_obtained) AS pass_count FROM result WHERE marks_obtained>40;

SELECT * FROM result WHERE marks_obtained>40 AND marks_obtained<70;
SELECT * FROM result WHERE marks_obtained<40 OR marks_obtained>80;
SELECT * FROM result WHERE subject='Math'AND (marks_obtained<40 OR marks_obtained>80);
SELECT * FROM result WHERE NOT marks_obtained>40;
SELECT * FROM result WHERE marks_obtained<>68;

-- other aggregate functions sum(), average()
