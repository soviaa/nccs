-- Copy database from remote to local 
-- Create a backup of remote database
-- mysqldump -u student -p -h 34.87.139.140 practice > practice.sql

-- login to local mysql server and create a new database
CREATE DATABASE practice_restore;

-- logout and restore
-- mysql -u root -p practice_restore < practice.sql


-- Insert data on ecommerce database tables
INSERT INTO categories(name) VALUES('Home and appliences'), ('Grocessories'), ('Clothing and Shoes'), ('Electronics'), ('Kids and Gifts');

INSERT INTO products (name, code, unit, cost_price, selling_price, stock, category_id) VALUES
    ('Gel Pen', 'HAPEN', 'pcs', 20, 30, 100, 1),
    ('Calculator', 'HACAL', 'pcs', 1000, 15000, 20, 4);
INSERT INTO users(name, address, email, phone) VALUES
    ('user name1', 'Chabahil, kathmandu', 'user1@gmail.com', '98400651343'),
    ('user name2', 'Sanepa, kathmandu', 'user2@gmail.com', '98400651378');

-- Create a order record
INSERT INTO orders(user_id, order_date, order_status) VALUES
    (1, now(), 'Initial');
-- Add some product into that order
INSERT INTO orders_products(order_id, product_id, quantity, discount) VALUES
    (1, 1, 5, 10),
    (1, 2, 1, 5);

-- To see which user ordered what products on which quantity
SELECT u.name, p.name, op.quantity, op.discount FROM orders_products op, orders o, products p, users u WHERE u.id = o.user_id AND o.id = op.order_id AND p.id = op.product_id;

-- Use Inner, left and right joins
-- Using inner join
SELECT u.name, p.name, op.quantity, op.discount FROM orders_products op JOIN orders o ON op.order_id = o.id JOIN products p ON op.product_id = p.id JOIN users u ON o.user_id = u.id;



