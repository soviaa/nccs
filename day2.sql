-- login using root
-- mysql -u root -p

CREATE USER 'username'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'username'@'localhost';
exit;

-- login using your username
-- mysql -u username -p
-- provide your password

CREATE USER 'guest'@'localhost' IDENTIFIED BY 'guestpassword';
GRANT SELECT ON practice.* TO 'guest'@'localhost'; -- username user don't have grant privileges
SELECT user();
exit;

--login useing root
GRANT GRANT OPTION ON *.* TO 'username'@'localhost';
exit;

--login using username
GRANT SELECT ON practice.* TO 'guest'@'localhost';
exit;

-- login using guest
SHOW databases;
USE practice;
SELECT * FROM product;
UPDATE product SET quantity = 20; -- guest don't have update privileges

--login using username
select host, user, select_priv, insert_priv, update_priv from mysql.user;

USE practice;

CREATE TABLE result(ID INT NOT NULL auto_increment, PRIMARY KEY (id), subject VARCHAR(50), student_rollno VARCHAR(20), section CHAR, examdate DATE DEFAULT now(), marks_obtained INT DEFAULT 0, percentage DECIMAL(5,2) DEFAULT 0, passed BOOLEAN DEFAULT false );

INSERT INTO result(subject, student_rollno, section)
            VALUES('Database', 'BIM 430', 'A'),
                ('English', 'BIM 430', 'A');

SELECT * FROM result;

