// Basic schema for a simple e-commerce application
Categories: {
    id: int,
    name: varchar(20)
}
CREATE TABLE categories(id INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(20) NOT NULL);

Products: {
    id: int,
    name: varchar(20),
    code: varchar(20),
    unit: varchar(20),
    cost_price: float,
    selling_price: float,
    stock: int,
    category_id: int
}
CREATE TABLE products (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    code VARCHAR(20) UNIQUE NOT NULL,
    unit VARCHAR(20),
    cost_price float,
    selling_price float,
    stock int,
    category_id int,
    FOREIGN KEY (category_id) REFERENCES categories(id)
);

Users: {
    id: int,
    name: varchar(30),
    address: varchar(30),
    email: varchar(30),
    phone: varchar(20)
}
CREATE TABLE users(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    address VARCHAR(30),
    email VARCHAR(30) UNIQUE NOT NULL,
    phone VARCHAR(20) UNIQUE NOT NULL
);

Orders: {
    id: int,
    user_id: int,
    order_date: date,
    order_status: varchar(20),
}
CREATE TABLE orders(
    id INT PRIMARY KEY AUTO_INCREMENT,
    user_id INT NOT NULL,
    order_date DATE NOT NULL,
    order_status VARCHAR(20),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

Orders_Products: {
    order_id: int,
    product_id: int,
    quantity: int,
    discount: float
}
CREATE TABLE orders_products (
    order_id INT NOT NULL,
    product_id INT NOT NULL,
    quantity INT,
    discount FLOAT,
    PRIMARY KEY (order_id, product_id),
    FOREIGN KEY (order_id) REFERENCES orders(id),
    FOREIGN KEY (product_id) REFERENCES products(id)
);

