CREATE TABLE Invoice (
    ID int PRIMARY KEY,
    studentID int NOT NULL,
    amount float,
    remarks varchar(10),
    FOREIGN KEY (studentID) REFERENCES Student(ID)
);


ALTER TABLE result
ADD FOREIGN KEY (studentID) REFERENCES Student(ID);

ALTER TABLE student
DROP FOREIGN KEY FK_StudentResult;

CREATE TABLE Employees (
    ID int PRIMARY KEY,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int CHECK (Age>=18)
);

ALTER TABLE Result
ADD CHECK (marks_obtained>=0 AND marks_obtained<=100);

ALTER TABLE Employees
DROP CHECK CHK_EmployeesAge;
